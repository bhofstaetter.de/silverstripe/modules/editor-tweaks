# Silverstripe Editor Tweaks

Opinionated improvements for Silverstripe's HTMLEditorField

## Installation

```
composer require bhofstaetter/editor-tweaks
```

## Contents

- cleaner cms editor
- predfined style formats
- style format configuration through yaml

## Configuration

See ``config.yml`` and refer to the included models readme file

## Included Modules

- [Phone Link](https://github.com/firebrandhq/silverstripe-phonelink)
- [HTMLEditorSrcSet](https://github.com/bigfork/htmleditorsrcset)

## Todo

- Imagesize presets (https://docs.silverstripe.org/en/4/developer_guides/forms/field_types/htmleditorfield/#image-size-pre-sets)
- FA Icons Picker or Shortcode
- Translations
- Simple Config
- content css

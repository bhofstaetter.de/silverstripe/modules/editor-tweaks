<?php

use bhofstaetter\EditorTweaks\TinyMCEConfigExtension;
use SilverStripe\Core\Manifest\ModuleLoader;
use SilverStripe\Forms\HTMLEditor\TinyMCEConfig;

$config = TinyMCEConfig::get('cms');

try {
    $config->setFolderName(TinyMCEConfig::config()->get('upload_folder_name'));
} catch (\Exception $e) {}

$moduleLoader = ModuleLoader::inst()->getManifest();
$cmsModule = $moduleLoader->getModule('silverstripe/cms');
$adminModule = $moduleLoader->getModule('silverstripe/admin');
$assetsAdmin = $moduleLoader->getModule('silverstripe/asset-admin');

$plugins = [
    'anchor' => null,
    'image' => null,
    'charmap' => null,
    'fullscreen' => null,
    'hr' => null,
    'contextmenu' => null,
    'searchreplace' => null,
    'wordcount' => null,
    'textpattern' => null,
    'paste' => null,
    'sslinkinternal' => $cmsModule->getResource('client/dist/js/TinyMCE_sslink-internal.js'),
    'sslinkanchor' => $cmsModule->getResource('client/dist/js/TinyMCE_sslink-anchor.js'),
    'sslink' => $adminModule->getResource('client/dist/js/TinyMCE_sslink.js'),
    'sslinkexternal' => $adminModule->getResource('client/dist/js/TinyMCE_sslink-external.js'),
    'sslinkemail' => $adminModule->getResource('client/dist/js/TinyMCE_sslink-email.js'),
    'sslinkfile' => $assetsAdmin->getResource('client/dist/js/TinyMCE_sslink-file.js'),
    'ssembed' => $assetsAdmin->getResource('client/dist/js/TinyMCE_ssembed.js'),
];

$options = [
    'friendly_name' => 'Standard Editor',
    'skin' => 'silverstripe',
    'style_formats' => TinyMCEConfigExtension::parse_yaml_formats(),
    'paste_as_text' => true,
    'allow_script_urls' => true,
    'importcss_append' => true,
    'importcss_selector_filter' => 'none',
    'paste_plaintext_inform' => true,
    'contextmenu' => 'sslink anchor charmap ssmedia ssembed | inserttable cell row column deletetable | code',
    'valid_elements' => $config->getOption('valid_elements') . ',-small[class|align|style]',
];

$config
    ->setButtonsForLine(1,
        'styleselect', '|',
        'bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript', 'hr', '|',
        'alignleft', 'aligncenter', 'alignright', 'alignjustify', '|',
        'bullist', 'numlist', 'outdent', 'indent', '|',
        '', '|',
        'pastetext', 'removeformat', 'fullscreen'
    )
    ->setButtonsForLine(2, '')
    ->setOptions($options)
    ->enablePlugins($plugins);
